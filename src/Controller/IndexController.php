<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class IndexController extends Controller
{
    /**
     * @Route("/register", name="register")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function RegisterAction(
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = [
                'email' => $user->getEmail(),
                'login' => $user->getLogin(),
                'password' => $user->getPassword()
            ];

            $userHandler->createNewUser($data);
            $user = $userHandler->createNewUser($data);
            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute("homepage");

        }

        return $this->render('register.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/", name="homepage")
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function indexAction( ApiContext $apiContext)

    {

        if(isset($_SESSION['q']) !==true or isset($_SESSION['sort']) !==true or isset($_SESSION['limit']) !==true ){
            $_SESSION['q'] ='programming';
            $_SESSION['sort'] = 'hot';
            $_SESSION['limit'] = '8';
        }
        $data = [
            'q' => $_SESSION['q'],
            'sort' => $_SESSION['sort'],
            'limit' => $_SESSION['limit'],
            'type'=>'link'
        ];
        $posts = $apiContext->getPostsFromReddit($data);
        dump($posts);
//        foreach ($posts['data']['children'] as $post){
//            $post['likes'] = $this->getDoctrine()->getRepository();
//        }
        return $this->render('index.html.twig', [
            "posts"=> $posts,
        ]);
    }

    /**
     * @Route("/q", name="search")
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function searchAction( ApiContext $apiContext)
    {
        $q = $_POST['q'];
        if( $_SESSION['sort'] = null || $_SESSION['limit'] = null ){
            $_SESSION['sort'] = 'hot';
            $_SESSION['limit'] = '8';
        }
        $_SESSION['q'] = $q;
        $data = [
            'q' => $_SESSION['q'],
            'sort' => $_SESSION['sort'],
            'limit' => $_SESSION['limit'],
            'type'=>'link'
        ];
        $posts = $apiContext->getPostsFromReddit($data);
        return $this->render('index.html.twig', [
            "posts"=> $posts
        ]);
    }

    /**
     * @Route("/limit_sort_{limit}", name="sort_by_limit")
     * @param ApiContext $apiContext
     * @param $limit
     * @return Response
     * @throws ApiException
     */
    public function sortByNumberAction( ApiContext $apiContext, $limit)
    {
        $_SESSION['limit'] = $limit;
        $data = [
            'q' => $_SESSION['q'],
            'sort' => $_SESSION['sort'],
            'limit' => $_SESSION['limit'],
            'type'=>'link'
        ];
        $posts = $apiContext->getPostsFromReddit($data);
        return $this->render('index.html.twig', [
            "posts"=> $posts
        ]);
    }

    /**
     * @Route("/sort_{sort}", name="sort")
     * @param ApiContext $apiContext
     * @param $sort
     * @return Response
     * @throws ApiException
     */
    public function sortAction( ApiContext $apiContext, $sort)
    {
        $_SESSION['sort'] = $sort;
        $data = [
            'q' => $_SESSION['q'],
            'sort' => $_SESSION['sort'],
            'limit' => $_SESSION['limit'],
            'type'=>'link'
        ];
        $posts = $apiContext->getPostsFromReddit($data);
        return $this->render('index.html.twig', [
            "posts"=> $posts
        ]);
    }


    /**
     * @Route("/more_{post_id}", name="more")
     * @param ApiContext $apiContext
     * @param $post_id
     * @return Response
     * @throws ApiException
     */
    public function moreAction( ApiContext $apiContext, $post_id)
    {
        $data = [
            'id' => $post_id,
        ];
        $post = $apiContext->getPostInfo($data);
        dump($post);
        return $this->render('post.html.twig', [
            "post"=> $post
        ]);
    }


    /**
     * @Route("/login", name="login")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     */
    public function loginAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $error = null;
        $form = $this->createForm("App\Form\AuthType");
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );
            if ($user) {
                $userHandler->makeUserSession($user);
                return $this->render(
                    '/my_page.html.twig', []);
            }
        }
        return $this->render(
            '/login.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }
}
