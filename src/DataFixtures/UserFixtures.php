<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = $this->userHandler->createNewUser([
            'email' => 'email@gmail.com',
            'login' => 'user1',
            'password' => 'user1',
        ]);
        $user2 = $this->userHandler->createNewUser([
            'email' => 'email2@gmail.com',
            'login' => 'user2',
            'password' => 'user2',
        ]);
        $user3 = $this->userHandler->createNewUser([
            'email' => 'email3@gmail.com',
            'login' => 'user3',
            'password' => 'user3',
        ]);
        $manager->persist($user1);
        $manager->persist($user2);
        $manager->persist($user3);
        $manager->flush();
    }
}
