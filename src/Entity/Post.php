<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity("post_id")
 */
class Post
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */

    private $post_id;

    /**
     * @var User[]
     *
     * @ORM\ManyToMany(targetEntity="User")
     */
    private $users;



    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User $user
     * @return Post
     */
    public function addUser(User $user)
    {
        $this->users->add($user);
        return $this;
    }

    /**
     * @param string $post_id
     * @return Post
     */
    public function setPostId(string $post_id): Post
    {
        $this->post_id = $post_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostId(): string
    {
        return $this->post_id;
    }
}
