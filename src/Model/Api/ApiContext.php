<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client/email/{email}';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{encodedPassword}/{email}';

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function getPostsFromReddit($data)
    {
        return $this->makeQuery('https://www.reddit.com/r/picture/search.json',
            self::METHOD_GET, $data);
    }

    /**
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function getPostInfo($data)
    {
        return $this->makeQuery('https://www.reddit.com/api/info.json',
            self::METHOD_GET, $data);
    }
 }